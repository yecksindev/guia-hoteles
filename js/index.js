$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 3000
    });

    $('#modal-contacto').on('show.bs.modal',function(e){
        console.log("Modal contacto se está mostrando");
        $('#contactoBtn').removeClass('btn-outline-primary');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled',true);
        console.log("** Botón contacto desactivado **");
    })

    $('#modal-contacto').on('shown.bs.modal',function(e){
        console.log("Modal contacto se mostró");
    })

    $('#modal-contacto').on('hide.bs.modal',function(e){
        console.log("Modal contacto se oculta");
        $('#contactoBtn').removeClass('btn-secondary');
        $('#contactoBtn').addClass('btn-outline-primary');
        $('#contactoBtn').prop('disabled',false)
        console.log("** Botón contacto activado **");
    })

    $('#modal-contacto').on('hidden.bs.modal',function(e){
        console.log("Modal contacto se ocultó");

        $('#contactoBtn').prop('disabled',false)
    })




    $('#modal-reserva').on('show.bs.modal',function(e){
        console.log("Modal reserva se está mostrando");
        $('.btnr').removeClass('btn-primary');
        $('.btnr').addClass('btn-secondary');
        $('.btnr').prop('disabled',true);
    })

    $('#modal-reserva').on('shown.bs.modal',function(e){
        console.log("Modal reserva se mostró");
    })

    $('#modal-reserva').on('hide.bs.modal',function(e){
        console.log("Modal reserva se oculta");

       
        $('.btnr').removeClass('btn-secondary');
        $('.btnr').addClass('btn-primary');
        $('.btnr').prop('disabled',false)
    })

    $('#modal-reserva').on('hidden.bs.modal',function(e){
        console.log("Modal reserva se ocultó");

       
    })



})



